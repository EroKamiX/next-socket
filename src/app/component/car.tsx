'use client';
import { PropsWithChildren } from 'react';

type CarComponentProp = PropsWithChildren<{
    car_id?: any;
    number?: number;
    name?: string;
    teeoff?: string;
    isOnline?: boolean;
    socket: any;
}>;

export const CarComponent = ({ car_id, name, socket, teeoff, isOnline }: CarComponentProp) => {
    const turnOnLight = (_car_id: any) => {
        console.log('turnOnLight', _car_id);
        socket.emit('turnOnLight', { car_id: _car_id });
    };
    const turnOffLight = (_car_id: any) => {
        console.log('turnOffLight', _car_id);

        socket.emit('turnOffLight', { car_id: _car_id });
    };
    const soundWarningStop = (_car_id: any) => {
        console.log('soundWarningStop', _car_id);

        socket.emit('soundWarningStop', { car_id: _car_id });
    };
    return (
        <div style={{ marginLeft: 15, padding: 5, width: '100%' }}>
            <h3>
                <span>xe số {name}</span> <span>LTO: {teeoff}</span>{' '}
                <span style={{ color: isOnline ? 'green' : 'red' }}>{isOnline ? 'online' : 'offline'}</span>
            </h3>
            <button
                type="submit"
                style={{ marginLeft: 15, marginTop: 5, padding: 5, backgroundColor: 'white', color: 'black' }}
                onClick={() => turnOnLight(car_id)}>
                bật đèn
            </button>
            <button
                type="submit"
                style={{ marginLeft: 15, marginTop: 5, padding: 5, backgroundColor: 'white', color: 'black' }}
                onClick={() => turnOffLight(car_id)}>
                tất đèn
            </button>
            <button
                type="submit"
                style={{ marginLeft: 15, marginTop: 5, padding: 5, backgroundColor: 'white', color: 'black' }}
                onClick={() => soundWarningStop(car_id)}>
                Cảnh báo Dừng
            </button>
        </div>
    );
};
