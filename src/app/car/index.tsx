'use client';
import { useEffect, useState } from 'react';
import { io } from 'socket.io-client';
import axios from 'axios';
import { CarComponent } from '../component/car';

function useSocket() {
    const [socket, setSocket] = useState<any>(null);
    const token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOm51bGwsInJvbGUiOiIiLCJ0b2tlbklkIjoiYzg3OGEyM2YtZjY1Yi00MTgxLWJhNzEtNWUzYmQ4MGFlMTFlIiwiaWF0IjoxNzE3NTM0ODY1LCJleHAiOjE3MTc1NzA4NjV9.2qWvAe3SM3wf2V5BdoQT33cNjf9NVU-wsjVDn0PmgO4';
    useEffect(() => {
        const socketIo = io('https://socketio-sgms.dragondoson.vn', {
            transports: ['websocket'],
            auth: {
                token: 'Bearer ' + token,
            },
        });
        // const socketIo = io('https://socket.autogolf.vn/', { transports: ['websocket'] });
        // const socketIo = io('https://socketio-sgms.dragondoson.vn/', { transports: ['websocket'] });

        setSocket(socketIo);

        function cleanup() {
            socketIo.disconnect();
        }
        return cleanup;

        // should only run once and not on every re-render,
        // so pass an empty array
    }, []);

    return socket;
}

export default function Home() {
    const [cars, setCars] = useState<any[]>([]);
    const [isConnect, setIsConnect] = useState<boolean>(false);
    const [monitoring, setMonitoring] = useState<any>();
    const socket = useSocket();
    let carTemp: any = [];
    const handleDisconnect = () => {
        socket.disconnect();
    };

    const handleSendMessage = () => {
        socket.emit('messages', `socketId ${socket.id} say hello`);
    };

    useEffect(() => {
        if (!isConnect) {
            console.log('isConnect', isConnect);
            if (socket) {
                socket.on('connect', () => {
                    setIsConnect(true);
                });
                socket.emit('user-connect', 'heloo it s me mario');
                socket.on('error', (data: any) => {
                    console.log(data);
                });
                socket.on('connect_error', (error: any) => {
                    console.log(error);
                });
                socket.on('disconnect', (data: any) => {
                    console.log(data);
                });
                socket.on('total-car', (data: any) => {
                    // console.log(data);
                    data = data.sort((a: { name: number }, b: { name: number }) => a.name - b.name);
                    const new_cars = data.map((e: any) => {
                        const car_data = carTemp.find((car: any) => e.deviceCode == car.deviceCode);
                        // console.log('car_data', carTemp);
                        if (car_data) {
                            return {
                                ...e,
                                online: car_data.online,
                                teeoff: car_data.teeoff,
                            };
                        } else {
                            return {
                                ...e,
                                online: false,
                                teeoff: null,
                            };
                        }
                    });
                    carTemp = new_cars;
                    // console.log('new_cars', new_cars);
                    setCars(new_cars);
                    // socket.off('total-car');
                });
                socket.on('monitoring-199', (data: any) => {
                    setMonitoring(JSON.parse(data));
                });
            }
        } else {
            const car: any = cars.find(c => c.deviceCode == monitoring?.imei);
            if (car) {
                car.online = true;
                // console.log(monitoring);
                if (car && monitoring.teeoff) {
                    car.teeoff = monitoring.teeoff;
                    console.log(monitoring);
                    try {
                        // axios
                        //     .post('http://127.0.0.1:3010/log-lto', {
                        //         lto: monitoring.teeoff,
                        //         log: `${monitoring.name},${monitoring.imei},${monitoring.acc},${monitoring.latitude},${monitoring.longitude},${monitoring.speed},${monitoring.pitId},${monitoring.pitType},${monitoring.pitCount},${monitoring.time}`,
                        //     })
                        //     .catch(error => console.log(error));
                    } catch (error) {}
                }
            }
        }
    }, [monitoring, socket]);
    return (
        <main className="flex min-h-screen flex-col p-24">
            <button
                type="submit"
                style={{ marginLeft: 15, marginTop: 5, padding: 5, backgroundColor: 'white', color: 'black' }}
                onClick={() => handleDisconnect()}>
                disconnect
            </button>
            <br></br>
            <button
                type="submit"
                style={{ marginLeft: 15, marginTop: 5, padding: 5, backgroundColor: 'white', color: 'black' }}
                onClick={() => handleSendMessage()}>
                send {cars.length}
            </button>
            <div className="grid" style={{ gridTemplateColumns: 'auto auto auto' }}>
                {cars.map((item, index) => {
                    // console.log(item);
                    return (
                        <CarComponent key={index} car_id={item.oo_id} name={item.name} teeoff={item.teeoff} isOnline={item.online} socket={socket} />
                    );
                })}
            </div>
        </main>
    );
}
