'use client';
import React, { useState } from 'react';
import { useRouter } from 'next/navigation';
import Home from './car';
const Login: React.FC = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const router = useRouter();
    const [isLogin, setIsLogin] = useState<boolean>(false);
    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        // Validate username and password
        if (!username || !password) {
            setError('Please enter both username and password.');
            return;
        }

        // Assuming you have an authentication logic here
        // For simplicity, I'm just checking if username is "admin" and password is "password"
        if (username === 'dragonlinks' && password === '123456') {
            // If authentication succeeds, redirect to dashboard
            setIsLogin(true);
        } else {
            setError('Invalid username or password.');
        }
    };

    return (
        <div>
            {/* {!isLogin ? (
                <div>
                    <h1>Hi hi</h1>
                    <form onSubmit={handleSubmit}>
                        <div>
                            <label>Username:</label>
                            <input type="text" value={username} onChange={e => setUsername(e.target.value)} />
                        </div>
                        <div>
                            <label>Password:</label>
                            <input type="password" value={password} onChange={e => setPassword(e.target.value)} />
                        </div>
                        {error && <div style={{ color: 'red' }}>{error}</div>}
                        <button type="submit">Login</button>
                    </form>
                </div>
            ) : (
                <div>
                    <Home />
                </div>
            )} */}
            <div>
                <Home />
            </div>
        </div>
    );
};

export default Login;
